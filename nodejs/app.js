process.chdir(__dirname);

var express = require("express");
var nconf = require('nconf');
var shelljs = require('shelljs');
var request = require('request');
var registrar = "http://192.168.1.1:3001";
//var registrar = "http://localhost:3001";

var blockchain = require('./blockchain');

var energy = require('./energy');
var producer = require('./producer');

var app = express();

// read static configuration
var config = new nconf.Provider();
config.use('file', { file: 'config/config.json' });
config.load();

var buildingName = config.get('buildingName');
var walletAddress = config.get('walletAddress');
var typeName = config.get('type');

// configure view engine to render EJS templates.
app.set('views', __dirname + '/views/pages');
app.set('view engine', 'ejs');

app.use(express.static('public'));

// define routes.
app.get('/',
    function (req, res) {
        res.render('index', { type: typeName, building: buildingName });
    }
);

app.get('/about',
    function (req, res) {
        res.render('about', { building: buildingName });
    }
);

app.get('/settings',
    function (req, res) {
        res.render('settings', { building: buildingName });
    }
);

app.get('/auctionhouse',
    function (req, res) {
        var auctionHouseAbi = blockchain.getAbi(blockchain.auctionHouseABI);
        var auctionHouseAddress = blockchain.getContractAddress(blockchain.auctionHouseABI);
        console.log("AuctionHouseAddress: " + auctionHouseAddress);
        auctionHouseAddress = JSON.stringify(auctionHouseAddress);
        console.log("stringify "+ auctionHouseAddress);
        res.render('auctionhouse', { building: buildingName, contractAddress: auctionHouseAddress, abi: auctionHouseAbi });
    }
);

app.get('/energy/producer', function(req,res){
    var currentProduction = producer.getProducerContribution();
    currentProduction = JSON.stringify(currentProduction);
    res.send(currentProduction);
});

app.get('/reboot',
    function (req, res) {
        shelljs.exec('sudo pkill -INT geth');
        shelljs.exec('sleep 20');
        shelljs.exec('sudo reboot');
    }
);

app.get('/reset', function (req, res) {

    var callback = function (error, result) {
        console.log(JSON.stringify(result));
        res.redirect('http://localhost:3000');
    }

    blockchain.resetAuctionhouse(callback);
    }
);


app.get('/shutdown',
    function (req, res) {
        shelljs.exec('sudo pkill -INT geth');
        shelljs.exec('sleep 20');
        shelljs.exec('sudo shutdown -h now');
    }
);

app.get("/blockchain/balance/sunnytoken", function (req, res) {
    var callback = function (error, result) {
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }
    blockchain.getSunnyBalance(callback);

});

app.get("/blockchain/balance/chftoken", function (req, res) {
    var callback = function (error, result) {
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }
    blockchain.getCHFBalance(callback);

});
app.get("/blockchain/requestchf", function (req, res) {
    var callback = function (error, result) {
        res.send(result.body);
    }
    //only creator (registrar) of chf-token is allowed to issue new ones
    request.get(registrar + '/blockchain/generatechf', {
        form:{
            wallet : walletAddress,
            amount : 5 
        }
    }, callback);

});

app.get("/blockchain/mintsunnytoken", function (req, res) {
    console.log("Mint SunnyToken");
    var callback = function (error, result) {
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }
    blockchain.mintSunnyToken(callback);

});

app.get("/blockchain/destroysunnytoken", function (req, res) {
    console.log("Destroy SunnyToken");
    var callback = function (error, result) {
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }
    blockchain.destroySunnyToken(callback);

});

app.get("/blockchain/auctionhouse/queue", function (req, res) {
    var callback = function (error, result) {
        /* This data stack 1  */
        console.log(result);
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }
    blockchain.getAuctionHouseQueue(callback);

});
app.get("/blockchain/auctionhouse/reservetoken", function (req, res) {
    var callback = function (error, result) {
        /* This data stack 1  */
        console.log(result);
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }
    blockchain.reserveToken(buildingName,callback);

});
app.get("/blockchain/auctionhouse/selltoken", function (req, res) {
    var callback = function (error, result) {
        /* This data stack 1  */
        console.log(result);
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }
    //TODO flexible anzahl?
    blockchain.sellSunnyToken(1,buildingName, callback);

});
app.get("/blockchain/auctionhouse/buytoken/:id", function (req, res) {
    var callback = function (error, result) {
        /* This data stack 1  */
        console.log(result);
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }

    blockchain.buySunnyToken(req.params.id, callback);

});

app.get("")


app.use('/', energy);

app.use("*",
    function (req, res) {
        res.render('404');
    }
);

app.listen(3000, function () {
    console.log("Live at Port 3000");
});
