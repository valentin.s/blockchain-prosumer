var Web3 = require("web3");
var fs = require('fs');
var nconf = require('nconf');

var provider = "http://localhost:8545";
var CHFTokenABI = "contract_abi/CHFToken.json";
var RegistrarABI = "contract_abi/Registrar.json";
var networkID = 1337;

var config = new nconf.Provider();
config.use('file', { file: 'config/config.json' });
config.load();

var walletAddress  = config.get('walletAddress');
var walletPassword = config.get('walletPassword');

module.exports = {

    addOracle: function (newOracle, linkedWallet, callback) {
        initProvider();
        var Registrar = getContract(RegistrarABI).addOracle(newOracle, linkedWallet, callback);
    },
    generateCHFToken: function (generationAddress, amount,callback) {
        initProvider();
        var eventHandler = function (error, result) {
            generationCHFEvent.stopWatching();
            callback("", result.args.newBalance.toString());
        }
        var CHFToken = getContract(CHFTokenABI);
        var generationCHFEvent = CHFToken.CHFTokenGenerated({}, { fromBlock: 'latest', toBlock: 'latest', address: generationAddress });
        generationCHFEvent.watch(eventHandler);
        
        web3.personal.unlockAccount(walletAddress, walletPassword, 60);
        CHFToken.generateCHFToken(generationAddress, amount, { from: walletAddress, gas: 400000 });
        
    },
    getAbi: function (abi_json) {
        var parsed = JSON.parse(fs.readFileSync(abi_json));
        console.log("contract loaded: " + parsed.abi);
        return parsed.abi;
    },
    getContractAddress: function (abi_json) {
        var parsed = JSON.parse(fs.readFileSync(abi_json));
        console.log("contract address: " + parsed.networks[networkID].address);
        return parsed.networks[networkID].address;
    }
};

function initProvider() {
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        web3 = new Web3(new Web3.providers.HttpProvider(provider));
    }
    web3.eth.defaultAccount = walletAddress;
}
function getContract(abi_json) {
    var parsed = JSON.parse(fs.readFileSync(abi_json));
    var abi = parsed.abi;
    var address = parsed.networks[networkID].address;
    return web3.eth.contract(abi).at(address);
}