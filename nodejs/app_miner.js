process.chdir(__dirname);

var express = require("express");
var nconf = require('nconf');
var shelljs = require('shelljs');
const bodyParser = require("body-parser");
var registrar = require('./registrar');


var app = express();

// read static configuration
var config = new nconf.Provider();
config.use('file', { file: 'config/config.json' });
config.load();

var buildingName = config.get('buildingName');
var typeName = config.get('type');

// configure view engine to render EJS templates.
app.set('views', __dirname + '/views/pages');
app.set('view engine', 'ejs');

app.use(express.static('public'));

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

// define routes.
app.get('/',
    function (req, res) {
        res.render('index', { type: typeName, building: buildingName });
    }
);

app.get("/blockchain/generatechf", function (req, res) {
    var callback = function (error, result) {
        console.log(result);
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }
    registrar.generateCHFToken(req.body.wallet, req.body.amount, callback);

});



app.post("/blockchain/addoracle", function (req, res) {
    var callback = function (error, result) {
        console.log(result);
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    }
    //TODO fix post params
    registrar.addOracle(req.params.id, callback);

});


app.use("*",
    function (req, res) {
        res.render('404');
    }
);

app.listen(3001, function () {
    console.log("Live at Port 3001");
});