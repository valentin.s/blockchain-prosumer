var Web3 = require("web3");
var fs = require('fs');
var nconf = require('nconf');

var provider = "http://localhost:8545";
var SunnyTokenABI = "contract_abi/SunnyToken.json";
var CHFTokenABI = "contract_abi/CHFToken.json";
var AuctionHouseABI = "contract_abi/AuctionHouse.json";
var networkID = 1337;

var config = new nconf.Provider();
config.use('file', { file: 'config/config.json' });
config.load();

var walletAddress  = config.get('walletAddress');
var walletPassword = config.get('walletPassword');
var oracleAddress  = config.get('oracleAddress');
var oraclePassword = config.get('oraclePassword');
var buildingName = config.get('buildingName');


module.exports = {

    auctionHouseABI: AuctionHouseABI,

    getSunnyBalance: function (callback) {
        initProvider();
        getContract(SunnyTokenABI).balanceOf(walletAddress, callback);
    },
    getCHFBalance: function (callback) {
        initProvider();
        getContract(CHFTokenABI).balanceOf(walletAddress, callback);
    }
    ,
    getAuctionHouseQueue: function (callback) {
        initProvider();
        var AuctionHouse = getContract(AuctionHouseABI);
        var head = AuctionHouse.head();
        var tail = AuctionHouse.tail();
        console.log("Tail ist"+ tail);
        var element = AuctionHouse.getQueueElement.call(tail);
        var result = [];
        while (element[2] != 0) {
            result.push({ next: element[0], prev: element[1], address: element[2], creator: element[3], reservator: element[4], creatorName: element[5], reservatorName:element[6] });
            element = AuctionHouse.getQueueElement.call(element[1]);
        }
        callback("", result);
    },
    resetAuctionhouse: function(callback) {
        initProvider();
        var AuctionHouse = getContract(AuctionHouseABI);
        AuctionHouse.clearAuctionHouse({ from: walletAddress, gas: 800000 });
    },
    getFreeSunnyTokens : function(callback){
        var AuctionHouse = getContract(AuctionHouseABI);
        return AuctionHouse.freeTokens.call(callback);
    },
    sellSunnyToken: function (amount, buildingName, callback) {
        initProvider();

        var SunnyToken = getContract(SunnyTokenABI);
        //SunnyToken.approve(this.getContractAddress(AuctionHouseABI), amount);

        var watcher = function (error, result) {
            if(buildingName == result.args.creatorName){
                console.log("token sold")
                event.stopWatching();
                callback("", result);
            }
        }

        var AuctionHouse = getContract(AuctionHouseABI);
        var sellToken = function (error, result){
            console.log("Selling approved");
            sunnyApproval.stopWatching();
            console.log("Selling amount = " + amount);
            AuctionHouse.sellEnergyToken(amount,buildingName, { from: walletAddress, gas: 800000 });
        }

        var sunnyApproval = SunnyToken.Approval({}, { fromBlock: 'latest', toBlock: 'latest', address: walletAddress });
        sunnyApproval.watch(sellToken);

        var event = AuctionHouse.TokenOffered({}, { fromBlock: 'latest', toBlock: 'latest', address: walletAddress });
        event.watch(watcher);
      
         SunnyToken.approve(this.getContractAddress(AuctionHouseABI), amount+1);

    },
    reserveToken: function (buildingName, callback) {
        initProvider();

        var CHFToken = getContract(CHFTokenABI);
        var AuctionHouse = getContract(AuctionHouseABI);
        var filter;
        var hash;
        var counter = 100;
        var cb;
        var boolean = false;
        var blockWatcher = function(err, result){
            console.log("call wait for receipt");
            waitForReceipt(hash,cb);
            counter = counter - 1;
            console.log("error counter is "+ counter + "for blocknumber"+result.blockNumber);
            console.log("hash is"+hash);
            if(counter <= 0){
               cb("No transcation", "");
               filter.stopWatching();
            }
        }
        function waitForReceipt(hashParam, cbParam) {
            hash= hashParam;
            cb = cbParam;
            web3.eth.getTransactionReceipt(hash, function (err, receipt) {
            if (err) {
                console.log(err);
            }

            if (receipt !== null) {
              // Transaction went through
                  console.log("receipt is not null");
                  if (cb) {
                     filter.stopWatching();
                     console.log("transaction went through");
                     console.log("status was "+receipt.status);
                     cb("",receipt);
                  }
            } else {
                 if(!boolean){
                    filter = web3.eth.filter('latest');
                    filter.watch(blockWatcher);
                    boolean = true;
                 }
            }
            });
        }
        var event;
        var reservationWatcher = function(err, result){
            if(buildingName==result.args.reservatorName){
                callback("",result);
                event.stopWatching();
            }
        }

        var receiptResult = function(error, res){
            console.log("error is"+error);
            console.log("receipt status is" + res.status);
            if(res.status=='0x1'){
               event = AuctionHouse.TokenReserved({}, { fromBlock: res.hash, toBlock: 'latest', address: walletAddress });

               event.watch(reservationWatcher);
            }else{
               console.log("Error happened");
               callback("Transaction failed", "");
            }
        }

        var reserve = function(error, result){
            console.log("reserve function was called");
            AuctionHouse.reserveToken(buildingName, {from: walletAddress, gas: 400000}, watcher);
            console.log("reserve function executed")
            chfApproval.stopWatching();
        }
        var watcher = function (error, result) {
            console.log("Transaction happened");
            console.log("result is "+ result);
            var transcactionID = web3.toHex(result);
            waitForReceipt(transcactionID, receiptResult);

        }

        console.log("approved: " + CHFToken.approve(this.getContractAddress(AuctionHouseABI), 10));
        var chfApproval = CHFToken.Approval({}, { fromBlock: 'latest', toBlock: 'latest', address: walletAddress });
        chfApproval.watch(reserve);


    },
    buySunnyToken: function (id, callback) {
        initProvider();
        getContract(AuctionHouseABI).buyEnergyToken(id, { from: walletAddress, gas: 400000 });
        var watcher = function (error, result) {
            if(walletAddress==result.args.reservator){
                console.log("token bought");
                event.stopWatching();
                callback("", result);
            }
        }
        var event = getContract(AuctionHouseABI).TokenSold({}, { fromBlock: 'latest', toBlock: 'latest', address: walletAddress });
        event.watch(watcher);

    },
    mintSunnyToken: function (callback) {
        initProvider();
        getContract(SunnyTokenABI).mintToken({ from: oracleAddress, gas: 400000 },callback);

    },
    destroySunnyToken: function (callback) {
        initProvider();
        getContract(SunnyTokenABI).destroyToken({ from: oracleAddress, gas: 400000 },callback);
    },
    getAbi: function (abi_json) {
        var parsed = JSON.parse(fs.readFileSync(abi_json));
        console.log("contract loaded: " + parsed.abi);
        return parsed.abi;
    },
    getContractAddress: function (abi_json) {
        var parsed = JSON.parse(fs.readFileSync(abi_json));
        console.log("contract address: " + parsed.networks[networkID].address);
        return parsed.networks[networkID].address;
    }
};

function initProvider() {
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        web3 = new Web3(new Web3.providers.HttpProvider(provider));
    }
    web3.eth.defaultAccount = walletAddress;
}
function getContract(abi_json) {
    var parsed = JSON.parse(fs.readFileSync(abi_json));
    var abi = parsed.abi;
    var address = parsed.networks[networkID].address;
    return web3.eth.contract(abi).at(address);
}
function init(){
    console.log("Unlocker called");
    initProvider();
   // web3.personal.unlockAccount(walletAddress,walletPassword);
    console.log("Wallet unlocked");
   // web3.personal.unlockAccount(oracleAddress,oraclePassword);
    console.log("Oracle unlocked");
}

init();
