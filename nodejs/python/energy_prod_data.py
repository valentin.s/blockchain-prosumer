## energy_prod_data.py

import sys, json, datetime
from ina219 import INA219

ina = INA219(shunt_ohms=0.1,
             max_expected_amps = 0.6,
             address=0x41)

ina.configure(voltage_range=ina.RANGE_16V,
              gain=ina.GAIN_AUTO,
              bus_adc=ina.ADC_128SAMP,
              shunt_adc=ina.ADC_128SAMP)


def getEnergyProd():

  u = ina.voltage()
  i = ina.current()
  p = ina.power()

  p = p * 500

  if p > 1000:
    p = 1000

  now = datetime.datetime.now()
  time = str(now)[:19]

  data = { "timestamp": time, "power": p }
  result = json.dumps( data )
  return sys.stdout.write(str(result))


#start process
if __name__ == '__main__':
    getEnergyProd()