## sim_prod_data.py

import sys, json, datetime, random

def getEnergyProd():

  now = datetime.datetime.now()
  time = str(now)[:19]
  power = random.random()
  
  data = { "timestamp": time, "power": power }
  result = json.dumps( data )
  return sys.stdout.write(str(result))


#start process
if __name__ == '__main__':
    getEnergyProd()