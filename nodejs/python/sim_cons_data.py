## sim_cons_data.py

import sys, json, datetime, random

def getEnergyCons():

  now = datetime.datetime.now()
  time = str(now)[:19]
  power = random.random()
  
  data = { "timestamp": time, "power": power }
  result = json.dumps( data )
  return sys.stdout.write(str(result))


#start process
if __name__ == '__main__':
    getEnergyCons()