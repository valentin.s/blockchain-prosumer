var Gpio = require('onoff').Gpio;

module.exports = {
    setRedLedBar: function (level, type) {
        var LED5 = new Gpio(17, 'out');
        var LED4 = new Gpio(27, 'out');
        var LED3 = new Gpio(22, 'out');
        var LED2 = new Gpio(23, 'out');
        var LED1 = new Gpio(18, 'out');

        if (type == 'prosumer') {
            if (level > 0) LED5.writeSync(1); else LED5.writeSync(0);
            if (level > 1) LED4.writeSync(1); else LED4.writeSync(0);
            if (level > 2) LED3.writeSync(1); else LED3.writeSync(0);
            if (level > 3) LED2.writeSync(1); else LED2.writeSync(0);
            if (level > 4) LED1.writeSync(1); else LED1.writeSync(0);
        } else {
            if (level > 0) LED1.writeSync(1); else LED1.writeSync(0);
            if (level > 1) LED2.writeSync(1); else LED2.writeSync(0);
            if (level > 2) LED3.writeSync(1); else LED3.writeSync(0);
            if (level > 3) LED4.writeSync(1); else LED4.writeSync(0);
            if (level > 4) LED5.writeSync(1); else LED5.writeSync(0);
        }
    },

    setGreenLedBar: function (level, type) {
        console.log("Level Green Bar is"+level);
        var LED5 = new Gpio(19, 'out');
        var LED4 = new Gpio(26, 'out');
        var LED3 = new Gpio(21, 'out');
        var LED2 = new Gpio(20, 'out');
        var LED1 = new Gpio(16, 'out');

        if (type == 'prosumer') {
            if (level > 0) LED5.writeSync(1); else LED5.writeSync(0);
            if (level > 1) LED4.writeSync(1); else LED4.writeSync(0);
            if (level > 2) LED3.writeSync(1); else LED3.writeSync(0);
            if (level > 3) LED2.writeSync(1); else LED2.writeSync(0);
            if (level > 4) LED1.writeSync(1); else LED1.writeSync(0);
        } else {
            if (level > 0) LED1.writeSync(1); else LED1.writeSync(0);
            if (level > 1) LED2.writeSync(1); else LED2.writeSync(0);
            if (level > 2) LED3.writeSync(1); else LED3.writeSync(0);
            if (level > 3) LED4.writeSync(1); else LED4.writeSync(0);
            if (level > 4) LED5.writeSync(1); else LED5.writeSync(0);
        }
    },

    setRedLedStrip: function(active) {
        var led = new Gpio(6, 'out');
        led.writeSync(active);
    },
    setGreenLedStrip: function(active) {
        var led = new Gpio(13, 'out');
        led.writeSync(active);
    },
    setWhiteLed: function(active) {
        var led = new Gpio(5, 'out');
        led.writeSync(active);
    }
}
