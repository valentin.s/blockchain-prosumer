var express = require('express');
var nconf   = require('nconf');
var fs      = require('fs');

var blockchain = require('./blockchain');
var led        = require('./led');
var producer   = require('./producer');

var production  = { "power": 0 };
var consumption = { "power": 0 };

var nettoEnergy = 0;

var pendingPurchase = false;
var pendingReservation = false;
var reservedTokenId = "";

var sunnyTokenBalance = 0;
var chfTokenBalance = 0;

var energyToTokenFactor = 0.025;

var config = new nconf.Provider();
config.use('file', { file: 'config/config.json' });
config.load();

var buildingName = config.get('buildingName');
var type = config.get('type');

fs.writeFileSync('data/energy_data.json', fs.readFileSync('data/energy_data.orig'));

var store = new nconf.Provider();
store.use('file', { file: 'data/energy_data.json' });
store.load();

var prodEnergyTotal = 0;
var consEnergyTotal = 0;
store.set('prod-total', prodEnergyTotal);
store.set('cons-total', consEnergyTotal);

var consTokenCounter  = 1000;
var prodTokenCounter  = 0;
store.set('prod-token', prodTokenCounter);
store.set('cons-token', consTokenCounter);


module.exports = (function() {
    'use strict';

    var router = require('express').Router();

    router.get('/energy/production', function (req, res) {
        var prod = JSON.stringify(production);
        res.end(prod);
    }),

    router.get('/energy/consumption', function (req, res) {
        var cons = JSON.stringify(consumption);
        res.end(cons);
    }),

    router.get('/energy/token/production', function (req, res) {
        var prod = JSON.stringify(parseInt(prodTokenCounter));
        res.end(prod);
    }),
    
    router.get('/energy/token/consumption', function (req, res) {
        var cons = JSON.stringify(parseInt(consTokenCounter));
        res.end(cons);
    }),

    router.get('/energy/total/production', function (req, res) {
        var prod = JSON.stringify({'prod_total' : parseFloat(prodEnergyTotal).toFixed(2)});
        res.end(prod);
    }),
    
    router.get('/energy/total/consumption', function (req, res) {
        var cons = JSON.stringify({'cons_total' : parseFloat(consEnergyTotal).toFixed(2)});
        res.end(cons);
    })

    return router;
})();


// prosumer/consumer - consumption
function energyConsCounter() {
    var spawn = require('child_process').spawn,
        py = spawn('python', ['python/energy_cons_data.py']);

    py.stdout.on('data', function (data) {
        consumption = JSON.parse(data.toString());
        consEnergyTotal += (parseFloat(consumption.power) / (10 * 1000));
        store.set('cons-total', consEnergyTotal);
        store.save();
    });

    py.stdin.write(JSON.stringify(consumption));
    py.stdin.end();
}

// prosumer - production
function energyProdCounter() {
    var spawn = require('child_process').spawn,
        py = spawn('python', ['python/energy_prod_data.py']);

    py.stdout.on('data', function (data) {
        production = JSON.parse(data.toString());
        prodEnergyTotal += (parseFloat(production.power) / (10 * 1000));
        store.set('prod-total', prodEnergyTotal);
        store.save();
    });

    py.stdin.write(JSON.stringify(production));
    py.stdin.end();
}

// producer - production
function energyDataCollector() {
    var  prod = 
        producer.energyConsumer1 +
        producer.energyConsumer2 -
        producer.energyProsumer1 -
        producer.energyConsumer2;
    production.power = prod;
    prodEnergyTotal += (parseFloat(prod) / (10 * 1000));
}

function nettoEnergyCounter() {
    var prodEnergy = parseFloat(production.power); 
    var consEnergy = parseFloat(consumption.power);

    function getLevel(energy) {
        if (energy == 0)   return 0;
        if (energy <  250) return 1;
        if (energy <  500) return 2;
        if (energy <  650) return 3;
        if (energy <  850) return 4;
        return 5;
    }

    var prodLevel = getLevel(prodEnergy);
    led.setGreenLedBar(prodLevel, type);

    var consLevel = getLevel(consEnergy);
    led.setRedLedBar(consLevel, type);
  

    nettoEnergy = prodEnergy - consEnergy;

    if (nettoEnergy > 0) {
        var prod = prodTokenCounter + nettoEnergy * energyToTokenFactor;
        prodTokenCounter = (prod > 1000) ? 1000 : prod;
        store.set('prod-token', prodTokenCounter);
        led.setGreenLedStrip(1);
        led.setRedLedStrip(0);
    } else if (nettoEnergy < 0) {
        var cons = consTokenCounter + nettoEnergy * energyToTokenFactor;
        consTokenCounter = (cons < 0) ? 0 : cons;
        store.set('cons-token', consTokenCounter);
        led.setGreenLedStrip(0);
        led.setRedLedStrip(1);
    } else {
        led.setGreenLedStrip(0);
        led.setRedLedStrip(0);
    }

    store.save();
}

function sellSunnyTokenTask() {
    if (prodTokenCounter == 1000) {
        prodTokenCounter = 0;
        store.set('prod-token', prodTokenCounter);

        var callbackMint = function (error, result) {
            try {
                blockchain.sellSunnyToken(1, buildingName, callbackSell);
            } catch (e) {
                console.log('sellSunnyToken: ' + e.Message);
            }
        }

        var callbackSell = function (error, result) {
            console.log(result);
        }

        try {
            blockchain.mintSunnyToken(callbackMint);
        } catch (e) {
            console.log('mintSunnyToken: ' + e.Message);
        }
    }
}

function buySunnyTokenTask() {
    console.log('current token consumption is'+consTokenCounter);
    // reservation
    if (consTokenCounter < 400 && !pendingReservation && !pendingPurchase && reservedTokenId.length<1) {
        console.log('reservation needed');
        var callback = function (error, result) {
            if(error == ""){
                console.log('Token successfully reserved');
                reservedTokenId = result.args.addressToken;
                console.log('reservation id after call is '+ reservedTokenId);
                pendingReservation = false;
           }else{
                console.log(error);
                reservedTokenId = "";
                pendingReservation = false;
           }
        }

        try {
            blockchain.reserveToken(buildingName, callback);
            pendingReservation = true;
        } catch (e) {
            console.log('error: reserveToken: ' + e.Message);
        }
    }
    // purchase
    if ((consTokenCounter <= 0 || reservedTokenId.length>0) && !pendingPurchase && !pendingReservation) {
        console.log("consTokenCounter in purchase is "+consTokenCounter);
        console.log('buy token');

        var callbackDestroy = function (error, result) {
            pendingPurchase = false;
 	    consTokenCounter = 1000;
	    store.set('prod-token', consTokenCounter);
            reservedTokenId = "";
        }

        var callbackBuy = function (error, result) {
            console.log("attempting to destroy bought token");
            try {
                blockchain.destroySunnyToken(callbackDestroy);
            } catch (e) {
                console.log('destroySunnyToken: ' + e.Message);
            }
        }

        try {
            console.log("Before buy call");
            blockchain.buySunnyToken(reservedTokenId, callbackBuy);
            console.log('reservation TokenId for buy is'+ reservedTokenId);            
            pendingPurchase = true;
            reservedTokenId = "";
        } catch (e) {
            console.log('buySunnyToken: ' + e.Message);
        }
    }
}

function checkCHFBalance() {
    var callback = function (error, result) {
        console.log(result);
        chfTokenBalance = result;
        led.setWhiteLed(chfTokenBalance > 0 ? 1 : 0);
    }
    blockchain.getCHFBalance(callback);
}

function checkSunnyBalance() {
    var callback = function (error, result) {
        console.log(result);
        sunnyTokenBalance = result;
    }
    blockchain.getSunnyBalance(callback);
}

function init() {
    if (type == 'consumer' || type == 'prosumer') {
        setInterval(energyConsCounter, 1000);
        setInterval(buySunnyTokenTask, 1000);
        setInterval(nettoEnergyCounter, 1000);
    }
    if (type == 'prosumer') {
        setInterval(energyProdCounter,  1000);
        setInterval(sellSunnyTokenTask, 1000);
        setInterval(nettoEnergyCounter, 1000);

    }

    if (type == 'producer') {
        producer.init();
    }
}

init();
