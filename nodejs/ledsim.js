module.exports = {
    setRedLedBar: function (level) {
        console.log("RedLedBar: LEVEL " + level);
    },

    setGreenLedBar: function (level) {
        console.log("GreenLedBar: LEVEL " + level);
    },

    setRedLedStrip: function(active) {
        console.log("RedLedStrip: " + active);
    },
    setGreenLedStrip: function(active) {
        console.log("GreenLedStrip: " + active);
    },

    setWhiteLed: function(active) {
        console.log("WhiteLed: " + active);
    }
}