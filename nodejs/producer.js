var request     = require("request");
var blockchain  = require('./blockchain');
var express     = require('express');
var led         = require('./led');
var nconf = require('nconf');

var prodProsumer1 = 0;
var consProsumer1 = 0;
var prodProsumer2 = 0;
var consProsumer2 = 0;
var consumer1 = 0;
var consumer2 = 0;
var producedToken = 0;
var minTokens=4;
var pendingSell = false;

var prodMax = false;

var config = new nconf.Provider();
config.use('file', { file: 'config/config.json' });
config.load();
var walletAddress  = config.get('walletAddress');
var buildingName = config.get('buildingName');

module.exports = {
    energyProsumer1: (prodProsumer1 - consProsumer1),
    energyProsumer2: (prodProsumer2 - consProsumer2),
    energyConsumer1: (-consumer1),
    energyConsumer2: (-consumer2),
    productionMax: prodMax,
    init: function(){
        init();
    },
    getProducerContribution(){
	console.log("GET-Call:Produced Token is"+ producedToken);
        return producedToken;
    }
}
function getNumberFreeBlockchainTokens(){
    console.log("Checked if token is needed");
    var energyProsumer1 = (prodProsumer1 - consProsumer1);
    var energyProsumer2 = (prodProsumer2 - consProsumer2);
    var energyConsumer1 = (-consumer1);
    var energyConsumer2 = (-consumer2);
    var cbTokenProduction = function(err,res){
        console.log("res is"+res);
	console.log(res);
	if(buildingName==res.args.creatorName){
       	    pendingSell = false;
	    led.setGreenLedStrip(0);

	}
	// setTimeout(getNumberFreeBlockchainTokens,50000);
    }
    var callback = function(err,res){
        var tokenNeeded = minTokens;
        if(energyProsumer1>0){
            tokenNeeded--;
        }
        if(energyProsumer2>0){
            tokenNeeded--;
        }
        if(energyConsumer1>0){
            tokenNeeded--;
        }
        if(energyConsumer2>0){
            tokenNeeded--;
        }
        console.log("res=" + res + "tokenNeeded=" + tokenNeeded);
	producedToken = tokenNeeded - res;
        console.log("producedToken " + producedToken);
        console.log("pending sell "+ pendingSell);
        if(producedToken > 0 && !pendingSell){
            console.log(producedToken + "in Queue for Producer")
            led.setGreenLedBar(producedToken,"");
            led.setGreenLedStrip(1);
            blockchain.sellSunnyToken(producedToken,buildingName, cbTokenProduction);
	    pendingSell = true;
        }
    }
    blockchain.getFreeSunnyTokens(callback);
    setTimeout(getNumberFreeBlockchainTokens,5000);

}

function getEnergyProdProsumer1() {
    request.get("http://192.168.1.4:3000/energy/production", (error, response, data) => {
        if(error) {
            prodProsumer1 = 0;
            return console.dir(error);
        }
        prodProsumer1 = (JSON.parse(data).power);
    });
    setTimeout(getEnergyProdProsumer1, 3000);
}

function getEnergyConsProsumer1() {
    request.get("http://192.168.1.4:3000/energy/consumption", (error, response, data) => {
        if(error) {
            consProsumer1 = 0;
            return console.dir(error);
        }
        consProsumer1 = (JSON.parse(data).power);
    });
    setTimeout(getEnergyConsProsumer1, 3000);
}

function getEnergyProdProsumer2() {
    request.get("http://192.168.1.5:3000/energy/production", (error, response, data) => {
        if(error) {
            prodProsumer2 = 0;
            return console.dir(error);
        }
        prodProsumer2 = (JSON.parse(data).power);
    });
    setTimeout(getEnergyProdProsumer2, 3000);
}

function getEnergyConsProsumer2() {
    request.get("http://192.168.1.5:3000/energy/consumption", (error, response, data) => {
        if(error) {
            consProsumer2 = 0;
            return console.dir(error);
        }
        consProsumer2 = (JSON.parse(data).power);
    });
    setTimeout(getEnergyConsProsumer2, 3000);
}


function getEnergyConsumer1() {
    request.get("http://192.168.1.6:3000/energy/consumption", (error, response, data) => {
        if(error) {
            consumer1 = 0;
            return console.dir(error);
        }
        consumer1 = (JSON.parse(data).power);
    });
    setTimeout(getEnergyConsumer1, 3000);
}

function getEnergyConsumer2() {
    request.get("http://192.168.1.7:3000/energy/consumption", (error, response, data) => {
        if(error) {
            consumer2 = 0;
            return console.dir(error);
        }
        consumer2 = (JSON.parse(data).power);
    });
    setTimeout(getEnergyConsumer2, 3000);
}

function getCurrentMarketQueue() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/blockchain/auctionhouse/queue",
        timeout: 100000,
        success: function (data) {
            if (data.length < 5) {
                productionMax = true;
            } else {
                productionMax = false;
            }
        },
        error: function (parsedjson, textStatus, errorThrown) {
            console.log("getCurrentMarketQueue: error");
        },
        complete: function() {
            setTimeout(getCurrentMarketQueue, 1000);
        }
    });
}

function init() {
    getEnergyConsProsumer1();
    getEnergyProdProsumer1();
    getEnergyConsProsumer2();
    getEnergyProdProsumer2();
    getEnergyConsumer1();
    getEnergyConsumer2();
    getNumberFreeBlockchainTokens()
}
