#!/bin/sh

# Geth starten
/usr/local/bin/geth --datadir="/home/pi/blockchain-prosumer/blockchain/ethdata" --networkid 1337 --rpc --rpcapi="db,eth,net,web3,personal" --rpccorsdomain "*" --bootnodes="enode://dc23cebee14dce92208f12d09230c2dc31fccc5b639529318fcda0c4df89a3850aa42e2381e4122e37ed3577bd102490800e60bc1efdbcdc5b0b6130618ba3d7@192.168.1.1:30301" &

# Node.js Applikation starten
/usr/bin/node /home/pi/blockchain-prosumer/nodejs/app.js &
