require('truffle-test-utils').init();
var CHFToken = artifacts.require("./contracts/CHFToken.sol");

contract('CHFToken', function(accounts) {
    it("should allow to generate tokens with the contract-creator", function() {
        var targetBalance;
        var amount = 10;
        var tokenInstance;

        return CHFToken.deployed().then(function(instance) {
            tokenInstance = instance;
            return instance.balanceOf(accounts[0]);
        }).then(function(balance) {
            targetBalance = balance.toNumber() + amount;
            return tokenInstance.generateCHFToken(accounts[0], amount);
        }).then(function(instance){
            return tokenInstance.balanceOf(accounts[0]);
        }).then(function(balance){
            assert.equal(balance.toNumber(), targetBalance, "New Tokens were not generated successfully");
        });
    });

    it("should throw an event when a new token is generated", function(){
        var amount = 10;
        var tokenInstance;
        var transactionID;
        var targetBalance;
        return CHFToken.deployed().then(function(instance) {
            tokenInstance = instance;
            return instance.balanceOf(accounts[0]);
        }).then(function(balance){
            targetBalance = balance.toNumber() + amount;
            return tokenInstance.generateCHFToken(accounts[0],amount);
        })
        .then(function(transaction){
            assert.web3Event(transaction, {
                event: 'CHFTokenGenerated',
                  args: {
                    generationAddress: accounts[0],
                    newBalance: targetBalance 
                }
              }, 'The event is emitted');
        });
    });
    
    it("should not allow any non creator to generate new tokens", function() {
        var targetBalance;
        var amount = 10;
        var tokenInstance;
        web3.eth.coinbase = accounts[1];
        web3.personal.unlockAccount(accounts[1],"hslu",15000);

        return CHFToken.deployed().then(function(instance) {
            tokenInstance = instance;
            return instance.balanceOf(accounts[1]);
        }).then(function(balance) {
            targetBalance = balance.valueOf();
            return tokenInstance.generateCHFToken(accounts[1], amount, {from: accounts[1]});
        }).then(function(instance){
                assert.fail('Expected throw not received');
        }).catch(function(error) {
            assert.isOk(error.toString(), "Expected an Error");
        });
    });
  
});