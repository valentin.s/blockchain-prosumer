require('truffle-test-utils').init();
var AuctionHouse = artifacts.require("./contracts/AuctionHouse.sol");
var CHFToken = artifacts.require("./contracts/CHFToken.sol");
var SunnyToken = artifacts.require("./contracts/SunnyToken.sol");
var reservatorName = "AuctionHouseReservationTest";
var creatorId = "AuctionHouseSellingTest";


contract('AuctionHouse', function(accounts) {
    it("should have an empty queue when initialized", function() {
        return AuctionHouse.deployed().then(function(instance) {
            assert(instance.head(), 0, "Head element is wrongfully set");
            assert(instance.tail(), 0, "Tail element is wrongfully set");
        });
    });

    it("should be linked to the correct tokens", function() {
        return AuctionHouse.deployed().then(function(instance){
            assert(instance.sunnyToken(), SunnyToken.address, "SunnyToken is not correctly linked");        
            assert(instance.CHFToken(), CHFToken.address, "CHFToken is not correctly linked");
        });
    });

    it("should allow the selling of a SunnyToken", function() {
        var tokenInstance;
        var sunnyInstance;
        var amount = 1;
        var startSize;
        var transactionID;
        return AuctionHouse.deployed().then(function(instance){
            tokenInstance = instance;
            startSize = instance.length();
            return SunnyToken.deployed();
        }).then(function(sunnyToken){
            sunnyInstance = sunnyToken;
            return sunnyInstance.approve(AuctionHouse.address, amount);
        }).then(function(sunnyToken){
            return tokenInstance.sellEnergyToken(amount,creatorId);           
        }).then(function(transaction){
            transactionID = transaction;
            return tokenInstance.head();
        }).then(function(headAddress){
            assert(tokenInstance.length(), startSize + amount, "No Token was sold");
            assert.web3Event(transactionID, {
                event: 'TokenOffered',
                  args: {
                    addressToken: headAddress,
                    creatorName: creatorId 
                }
              }, 'The event is emitted');
        });
    });
    
    it("should allow the reservation of a SunnyToken", function() {
        var tokenInstance;
        var chfInstance;
        var amount = 1;
        var startSize;
        var transactionID;
        return AuctionHouse.deployed().then(function(instance){
            tokenInstance = instance;
            startSize = instance.length();
            return CHFToken.deployed();
        }).then(function(CHFToken){
            chfInstance = CHFToken;
            return chfInstance.approve(AuctionHouse.address, amount);
        }).then(function(CHFToken){
            return tokenInstance.reserveToken(reservatorName);           
        }).then(function(transaction){
            assert(tokenInstance.length(), startSize + amount, "No Token was sold");
            transactionID = transaction;
            return tokenInstance.tail();
        }).then(function(tailAddress){
            assert.web3Event(transactionID, {
                event: 'TokenReserved',
                  args: {
                    addressToken: tailAddress,
                    creatorName: creatorId,
                    reservatorName: reservatorName
                }
              }, 'The event is emitted');
        });
    });

    it("should not allow anybody besides the reservator to claim the token", function() {
        var tokenInstance;
        var chfInstance;
        var amount = 1;
        var startSize;
        web3.personal.unlockAccount(accounts[1],"hslu",15000);

        var transactionID;
        return AuctionHouse.deployed().then(function(instance){
            tokenInstance = instance;
            return tokenInstance.length();
        }).then(function(length){
            startSize = length;
            return tokenInstance.tail();
        }).then(function(tailAddress){
            return tokenInstance.buyEnergyToken(tailAddress, {from:accounts[1]});           
        }).then(function(transaction){
            assert.fail('Expected throw not received');
        }).catch(function(error) {
            assert.isOk(error.toString(), "Expected an Error");
        });
    });

    it("should allow the finalization of a SunnyToken trade", function() {
        var tokenInstance;
        var chfInstance;
        var amount = 1;
        var startSize;

        var transactionID;
        return AuctionHouse.deployed().then(function(instance){
            tokenInstance = instance;
            return tokenInstance.length();
        }).then(function(length){
            startSize = length;
            return tokenInstance.tail();
        }).then(function(tailAddress){
            return tokenInstance.buyEnergyToken(tailAddress);           
        }).then(function(transaction){
            assert(tokenInstance.length(), startSize - amount, "No Token was sold");
            transactionID = transaction;
            return tokenInstance.tail();
        }).then(function(tailAddress){
            assert.web3Event(transactionID, {
                event: 'TokenSold',
                  args: {
                    addressToken: tailAddress,
                    creator: accounts[0],
                    reservator: accounts[0]
                }
              }, 'The event is emitted');
        });
    });

    it("should add new tokens at the head", function() {
        var tokenInstance;
        var sunnyInstance;
        var allowance = 10;
        var amount = 1;
        var firstHeadAddress;
        return AuctionHouse.deployed().then(function(instance){
            tokenInstance = instance;
            return SunnyToken.deployed();
        }).then(function(sunnyToken){
            sunnyInstance = sunnyToken;
            return sunnyInstance.approve(AuctionHouse.address, allowance);
        }).then(function(sunnyToken){
            return tokenInstance.sellEnergyToken(amount,creatorId);           
        }).then(function(transaction){
            return tokenInstance.head();
        }).then(function(headAddress){
            firstHeadAddress = headAddress;
            return tokenInstance.sellEnergyToken(amount,creatorId);
        }).then(function(headAddress){
            assert.notEqual(headAddress, firstHeadAddress, "Element was not added at head");
        });
    });

    it("should reserve tokens at the tail", function() {
        var tokenInstance;
        var chfInstance;
        var amount = 1;
        var startSize;
        var transactionID;
        return AuctionHouse.deployed().then(function(instance){
            tokenInstance = instance;
            startSize = instance.length();
            return CHFToken.deployed();
        }).then(function(CHFToken){
            chfInstance = CHFToken;
            return chfInstance.approve(AuctionHouse.address, amount);
        }).then(function(CHFToken){
            return tokenInstance.reserveToken(reservatorName);           
        }).then(function(transaction){
            assert(tokenInstance.length(), startSize + amount, "No Token was sold");
            transactionID = transaction;
            return tokenInstance.tail();
        }).then(function(tailAddress){
            assert.web3Event(transactionID, {
                event: 'TokenReserved',
                  args: {
                    addressToken: tailAddress,
                    creatorName: creatorId,
                    reservatorName: reservatorName
                }
              }, 'The event is emitted');
        });
    });

  
});