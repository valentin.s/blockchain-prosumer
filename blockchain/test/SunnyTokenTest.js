require('truffle-test-utils').init();
var SunnyToken = artifacts.require("./contracts/SunnyToken.sol");
var Registrar = artifacts.require("./contracts/Registrar.sol");

contract('SunnyToken', function(accounts) {
    it("should allow oracles to mint new tokens", function() {
        var tokenInstance;
        var targetBalance;
        var registrarInstance;
        var oracleAddress = "0xb53a3276a6410dd3090ba1ada04453d25b03fa7d";
        var linkedWallet = "0x37f4600a992c263192e37cf27dec4c7aff2ab074";
        web3.personal.unlockAccount(oracleAddress,"hslu",15000);
        var amount = 1;
        return Registrar.deployed().then(function(instance){
            registrarInstance = instance;
            return SunnyToken.deployed();
        }).then(function(instance) {
            tokenInstance = instance;
            return instance.balanceOf(linkedWallet);
        }).then(function(balance) {
            targetBalance = balance.toNumber() + amount;
            return tokenInstance.mintToken({from: oracleAddress});
        }).then(function(instance){
            return registrarInstance.isOracle(oracleAddress);
        }).then(function(boolean){
            assert.isTrue(boolean, "Specified Address is not an oracle");
            return registrarInstance.getLinkedWallet(oracleAddress);
        }).then(function(walletAddress){
            assert.equal(walletAddress, linkedWallet,"Wallet is not the linked one");
            return tokenInstance.balanceOf(linkedWallet);
        }).then(function(balance){
            assert.equal(balance.toNumber(), targetBalance, "No new Token was minted");
        });
    });

    it("should allow oracles to destroy tokens", function() {
        var tokenInstance;
        var targetBalance = 0;
        var oracleAddress = "0xb53a3276a6410dd3090ba1ada04453d25b03fa7d";
        var linkedWallet = "0x37f4600a992c263192e37cf27dec4c7aff2ab074";
        web3.personal.unlockAccount(oracleAddress,"hslu",15000);

        return SunnyToken.deployed().then(function(instance) {
            tokenInstance = instance;
            return instance.balanceOf(linkedWallet);
        }).then(function(balance) {
            assert.equal(balance.toNumber(), 1, "No Token in wallet");
            return tokenInstance.destroyToken({from: oracleAddress});
        }).then(function(instance){
            return tokenInstance.balanceOf(linkedWallet);
        }).then(function(balance){
            assert.equal(balance.toNumber(), targetBalance, "No new Token was minted");
        });
    });
    
    it("should not allow non-oracles to mint new tokens", function() {
        return SunnyToken.deployed().then(function(instance) {
            targetBalance = instance.valueOf();
            return tokenInstance.mintToken({from: accounts[0]});
        }).then(function(instance){
                assert.fail('Expected throw not received');
        }).catch(function(error) {
            assert.isOk(error.toString(), "Expected an Error");
        });
    });

    it("should not allow non-oracles to destroy tokens", function() {
        return SunnyToken.deployed().then(function(instance) {
            targetBalance = instance.valueOf();
            return tokenInstance.destroyToken({from: accounts[0]});
        }).then(function(instance){
                assert.fail('Expected throw not received');
        }).catch(function(error) {
            assert.isOk(error.toString(), "Expected an Error");
        });
    });
  
});