require('truffle-test-utils').init();
var Registrar = artifacts.require("./contracts/Registrar.sol");


contract('Registrar', function(accounts) {
    it("should have the miner as owner address", function() {
        return Registrar.deployed().then(function(instance) {
            assert(instance.owner(), accounts[0], "Miner is not the owner");
           
        });
    });

    it("should let owner add new oracles", function() {
        var newOracleAddress = "0x12345678909edf6791b1ab279083d90ef0a063194"
        var linkedWallet = "0x98765432109edf6791b1ab279083d90ef0a063194"
        var tokenInstance;
        return Registrar.deployed().then(function(instance) {
            tokenInstance = instance;
            return instance.isOracle(newOracleAddress);
        }).then(function(boolean){
            assert.isFalse(boolean, "Oracle already added");
            return tokenInstance.addOracle(newOracleAddress,linkedWallet);
        }).then(function(instance){
            return tokenInstance.isOracle(newOracleAddress);
        }).then(function(boolean){
            assert.isTrue(boolean, "Oracle could not be added");
        });
    });

    it("should allow to get the linked wallet of an oracle", function(){
        var oracleProsumer1 = "0x2ef995db69edf6791b1ab279083d90ef0a063194";
        var walletProsumer1 = "0x2ce42acb894725fc7f18c7988a6fcc9794d9dac8";
        return Registrar.deployed().then(function(instance){
            return instance.getLinkedWallet(oracleProsumer1);
        }).then(function(walletAddress){
            assert(walletAddress, walletProsumer1, "linked wallet does not match");
        });
    });

    it("should not allow creation of new oracle by non-owner", function() {
        var newOracleAddress =  "0x11111111109edf6791b1ab279083d90ef0a063194";
        var linkedWallet = "0x22222222209edf6791b1ab279083d90ef0a063194";
        var tokenInstance;
        web3.personal.unlockAccount(accounts[1],"hslu",15000);
        return Registrar.deployed().then(function(instance) {
            tokenInstance = instance;
            return instance.isOracle(newOracleAddress);
        }).then(function(boolean){
            assert.isFalse(boolean, "Oracle already added");
            return tokenInstance.addOracle(newOracleAddress,linkedWallet, {from:accounts[1]});
        }).then(function(instance){
            assert.fail('Expected throw not received');
        }).catch(function(error) {
            assert.isOk(error.toString(), "Expected an Error");
        });
    });

    it("should have all oracles initialized for the demonstrator", function() {
        var tokenInstance;
        var targetBalance;
        var oracleProsumer1 = "0x2ef995db69edf6791b1ab279083d90ef0a063194";
        var oracleProsumer2 = "0xb53a3276a6410dd3090ba1ada04453d25b03fa7d";
        var oracleConsumer1 = "0x88ccc75109bab42062e40d11f4af9ef0e488679e";
        var oracleConsumer2 = "0x3ee324b58d51d7be93191f2d332f14aa8bfed626";
        var oracleProducer  = "0xfe3836212edf6cc87427c8ca685a9d0b28394720";

        return Registrar.deployed().then(function(instance) {
            tokenInstance = instance;
            return tokenInstance.isOracle(oracleConsumer1);
        }).then(function(instance){
            assert.isTrue(instance);
            return tokenInstance.isOracle(oracleConsumer2);
        }).then(function(instance){
            assert.isTrue(instance);
            return tokenInstance.isOracle(oracleProsumer1);
        }).then(function(instance){
            assert.isTrue(instance);
            return tokenInstance.isOracle(oracleProsumer2);
        }).then(function(instance){
            assert.isTrue(instance);
            return tokenInstance.isOracle(oracleProducer);
        }).then(function(instance){
            assert.isTrue(instance);
        });
    });

    it("should allow to change the linked wallet of an oracle", function(){
        var oracleProsumer1 = "0x2ef995db69edf6791b1ab279083d90ef0a063194";
        var walletProsumer1 = "0x2ce42acb894725fc7f18c7988a6fcc9794d9dac8";
        var newWallet = "0x99999999894725fc7f18c7988a6fcc9794d9dac8";
        var tokenInstance;
        return Registrar.deployed().then(function(instance){
            tokenInstance = instance;
            return instance.getLinkedWallet(oracleProsumer1);
        }).then(function(walletAddress){
            assert(walletAddress, walletProsumer1, "linked wallet does not match");
            return tokenInstance.changeLinkedWallet(oracleProsumer1, newWallet);
        }).then(function(instance){
            return tokenInstance.getLinkedWallet(oracleProsumer1);
        }).then(function(walletAddress){
            assert(walletAddress, newWallet, "linked wallet was not changed");
        });
    });

  
});