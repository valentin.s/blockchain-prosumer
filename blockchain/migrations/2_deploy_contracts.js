var Registrar = artifacts.require("./contracts/Registrar.sol");
var SunnyToken = artifacts.require("./contracts/SunnyToken.sol");
var CHFToken = artifacts.require("./contracts/CHFToken.sol");
var AuctionHouse = artifacts.require("./contracts/AuctionHouse.sol");


module.exports = function(deployer){
	deployer.deploy(CHFToken).then(function(){
		return deployer.deploy(Registrar, web3.eth.coinbase);
	}).then(function(){
		return deployer.deploy(SunnyToken, Registrar.address);
	}).then(function(){
		return deployer.deploy(AuctionHouse, SunnyToken.address, CHFToken.address);
	});
};

