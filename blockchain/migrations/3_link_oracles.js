var Registrar = artifacts.require("./contracts/Registrar.sol");

module.exports = async function(callback) {
    let instance = await Registrar.deployed();
    //Konsument 1
    instance.addOracle("0x88ccc75109bab42062e40d11f4af9ef0e488679e","0x2773f066d34ce08cd737bbf6f7dfda4cbfa68414",{
        from: web3.eth.coinbase
    });
    //Konsument 2
    instance.addOracle("0x3ee324b58d51d7be93191f2d332f14aa8bfed626","0x78f2089720cdb4764a30e799b98fda0a9f59b2db",{
        from: web3.eth.coinbase
    });
    //Kraftwerk
    instance.addOracle("0xfe3836212edf6cc87427c8ca685a9d0b28394720","0x7bc25df2333bca50a6084903f276ff9ad6373037",{
        from: web3.eth.coinbase
    });
    //Prosumer 1
    instance.addOracle("0x2ef995db69edf6791b1ab279083d90ef0a063194","0x2ce42acb894725fc7f18c7988a6fcc9794d9dac8",{
        from: web3.eth.coinbase
    });
    //Prosumer 2
    instance.addOracle("0xb53a3276a6410dd3090ba1ada04453d25b03fa7d","0x37f4600a992c263192e37cf27dec4c7aff2ab074",{
        from: web3.eth.coinbase
    });
    return instance.owner();
}