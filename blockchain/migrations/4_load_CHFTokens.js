var CHFToken = artifacts.require("./contracts/CHFToken.sol");

module.exports = function(callback) {
    this.CHFToken
        .deployed()
        .then(instance => {
        //Konsument 1
        instance.transfer("0x2773f066d34ce08cd737bbf6f7dfda4cbfa68414",50,{
            from: web3.eth.coinbase
        });
        //Konsument 2
        instance.transfer("0x78f2089720cdb4764a30e799b98fda0a9f59b2db",50,{
            from: web3.eth.coinbase
        });
        //Kraftwerk
        instance.transfer("0x7bc25df2333bca50a6084903f276ff9ad6373037",50,{
            from: web3.eth.coinbase
        });
        // Prosumer 1
        instance.transfer("0x2ce42acb894725fc7f18c7988a6fcc9794d9dac8",50,{
            from: web3.eth.coinbase
        });
        //Prosumer 2
        instance.transfer("0x37f4600a992c263192e37cf27dec4c7aff2ab074",50,{
            from: web3.eth.coinbase
        });
        return "Accounts successfully loaded with CHFTokens";
    })
    .then(result => {
        console.log(result);
    })
    .catch(e => {
        console.log(e);
    });
}