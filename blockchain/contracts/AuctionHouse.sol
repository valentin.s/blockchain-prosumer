pragma solidity ^0.4.4;

/// definition of erc20 standard according to ethereum
contract ERC20 {

    /// @return total amount of tokens
    function totalSupply() constant returns (uint256 supply) {}

    /// @param _owner The address from which the balance will be retrieved
    /// @return The balance
    function balanceOf(address _owner) constant returns (uint256 balance) {}

    /// @notice send `_value` token to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint256 _value) returns (bool success) {}

    /// @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
    /// @param _from The address of the sender
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {}

    /// @notice `msg.sender` approves `_addr` to spend `_value` tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _value The amount of wei to be approved for transfer
    /// @return Whether the approval was successful or not
    function approve(address _spender, uint256 _value) returns (bool success) {}

    /// @param _owner The address of the account owning tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @return Amount of remaining tokens allowed to spent
    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {}

    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
    
}

///contract to trade tokens according to a fi/fo behavior
contract AuctionHouse{

	//single element of the double-linked list
    struct QueueElement{
        bytes32 next;
        bytes32 previous;
		bytes32 addressElement;
        address creator;
        address reservator;
		string creatorName;
		string reservatorName;
        
    }
	
    ERC20 public sunnyToken;
    ERC20 public CHFToken;
    uint public length;
    uint public freeTokens;
    bytes32 public head;
    bytes32 public tail;
    mapping (bytes32 => QueueElement) public tokenQueue;
	
	//event definitions
    event TokenReserved(bytes32 addressToken, string creatorName, string reservatorName);
	event TokenOffered(bytes32 addressToken, string creatorName);
	event TokenSold(bytes32 addressToken, address creator, address reservator);
	
	/// @notice constructor of the contract
	/// @param _sunnyToken ERC20 token that represents energy in our model
	/// @ param _CHFToken ERC20 token that represents money in our model
	constructor(address _sunnyToken, address _CHFToken){
	
		//connecting to the smart contracts according to their addresses 
        sunnyToken = ERC20(_sunnyToken);
        CHFToken = ERC20(_CHFToken);
    }
	
    
	/// @notice returns all information of a queue element based on the `_address` 
	/// @return array with all parts of the element
    function getQueueElement(bytes32 _address) public view 
    returns(
		//definition of what values will be returned
        bytes32 next,
        bytes32 previous,
		bytes32 addressElement,
        address creator,
        address reservator,
		string creatorName,
		string reservatorName
    ){
        QueueElement element = tokenQueue[_address];
        return (
			//matching element to the definition of what should be returned
            element.next,
            element.previous,
			element.addressElement,
            element.creator,
            element.reservator,
			element.creatorName,
			element.reservatorName); 
    }
    
    
    /// @notice puts an energy-token up to be sold by the auction house
	/// @param _amount amount of energy-tokens that should be sold
	/// @param _creatorName identifier of the producer that generated the electricity
    function sellEnergyToken(uint _amount, string _creatorName)  public{
        
        //check if there are enough tokens that can be transfered
        if(!(sunnyToken.allowance(msg.sender, this) >= _amount)) revert();
        //check if the transaction was received
        if(!sunnyToken.transferFrom(msg.sender, this, _amount)) revert();
        
        //Create Queue-Elements for each token sent
        while(_amount>0){
            //addTokenToQueue(msg.sender, _creatorName);
            address sender = msg.sender;    
            QueueElement oldHead = tokenQueue[head];
    		
    		//new element will be placed at the head of the queue
            QueueElement memory element = QueueElement(head,0, 0, sender,0,_creatorName, "");
            
    		bytes32 id = sha3(sender, length, block.timestamp);
    		element.addressElement = id;
            tokenQueue[id] = element;
            head = id;
            oldHead.previous = id;
            if(tail==0){
                tail = id;
            }
    		emit TokenOffered(id, element.creatorName);
            length++;
            freeTokens++;
            _amount--;
        }
    }
	
	/// @notice redeems a reservation to complete the purchase of an energy-token
	/// @param _reservedToken address of the element in the queue
    function buyEnergyToken(bytes32 _reservedToken) public{
        
        QueueElement boughtToken = tokenQueue[_reservedToken];
        
        //Only address that made the reservation can redeem it
        if(boughtToken.reservator != msg.sender) revert();
        
        sunnyToken.transfer(msg.sender,1);
        CHFToken.transfer(boughtToken.creator, 1);
        
        
        QueueElement prevToken = tokenQueue[boughtToken.previous];
        QueueElement nextToken = tokenQueue[boughtToken.next];
        
		
		emit TokenSold(boughtToken.addressElement, boughtToken.creator, boughtToken.reservator);
		
		if(length==1){
		    head=0;
		}
		
        //Remove the Element from the list
        prevToken.next = boughtToken.next;
        nextToken.previous = boughtToken.previous;
        tail = boughtToken.previous;
		
		
        
        //Set all values to default (delete is not possible in solidity)
        boughtToken.next = 0;
        boughtToken.previous = 0;
        boughtToken.addressElement = 0;
		boughtToken.creator = 0;
        boughtToken.reservator = 0;
        boughtToken.creatorName = "";
        boughtToken.reservatorName= "";
		
        
        length--;
    }
    
	/// @notice generates a new element in the queue
	/// @param _creator wallet-address that sent the energy-token
	/// @param _creatorName identifier of the energy-producer
    function addTokenToQueue(address _creator, string _creatorName) private{
	
        address sender = _creator;    
        QueueElement oldHead = tokenQueue[head];
		
		//new element will be placed at the head of the queue
        QueueElement memory element = QueueElement(head,0, 0, sender,0,_creatorName, "");
        
		bytes32 id = sha3(sender, length, block.timestamp);
		element.addressElement = id;
        tokenQueue[id] = element;
        head = id;
        oldHead.previous = id;
        if(tail==0){
            tail = id;
        }
		emit TokenOffered(id, element.creatorName);
        length++;
    }
    
	/// @notice reserves an energy-token with approved chf-tokens
	/// @param _reservatorName identifier of the buyer
    function reserveToken(string _reservatorName) public{
        
        //check if there are enough tokens that can be transfered
        if(!(CHFToken.allowance(msg.sender, this) >= 1)) revert();
        //check if the transaction was received
        if(!CHFToken.transferFrom(msg.sender, this, 1)) revert();
        //check if there are elements available
        if(length==0) revert();
        //check if there are unreserved tokens
        if(freeTokens==0) revert();
		
        QueueElement element = tokenQueue[tail];
        bytes32 result = tail;
        
        while(element.previous != 0 && element.reservator !=0){
            result = element.previous;
            element = tokenQueue[element.previous];
        }
        if(element.creator == 0) revert();
        freeTokens--;
        element.reservator = msg.sender;
		element.reservatorName = _reservatorName;
        emit TokenReserved(result, element.creatorName, element.reservatorName);
        
    }
    
    function clearAuctionHouse() public{
        length=0;
        freeTokens=0;
        head=0;
        tail=0;    
        
    
    }

}
