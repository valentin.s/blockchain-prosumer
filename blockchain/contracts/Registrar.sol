pragma solidity ^0.4.4;

/// contract to handle all the oracles of the model
contract Registrar {
	
	//new modifier for functions only the contract creator can access
	modifier isOwner() {
	   if (msg.sender != owner) {
			throw;
		}

		_; // continue executing rest of method body
	}
    address public owner;
    struct oracle {
        string location;
		address wallet;
        bool isValue;
    }
	constructor(address _owner){

		owner = _owner;
	}
	

    
    mapping (address => oracle) oracles;
    address[] public oracleAccs;
    
	/// @notice generates a record of a new oracle
	/// @param _oracleAddress address which will get oracle rights to the linked wallet
	/// @param _walletAddress wallet address which can be manipulated by the oracle
    function addOracle  (address _oracleAddress, address _walletAddress) isOwner public {
        var tempOracle = oracles[_oracleAddress];
        tempOracle.location = "HSLU";
		tempOracle.wallet = _walletAddress;
        tempOracle.isValue = true;
    }
	
	/// @notice changes the wallet which can be manipulated by the oracle
	/// @param _oracleAddress oracle which should be linked to a new wallet address
	/// @param _walletAddress new wallet address for the oracle
	function changeLinkedWallet (address _oracleAddress, address _walletAddress) isOwner public{
		var tempOracle = oracles[_oracleAddress];
		tempOracle.wallet = _walletAddress;
	}
    
	/// @notice provides information if a certain address is an oracle or notice
	/// @param _address address which should be checked
	/// @return wether the address is an oracle or not
    function isOracle(address _address) view public returns(bool){
        return oracles[_address].isValue;
    } 
	
	/// @notice gets the wallet-address based on the oracle
	/// @param _oracleAddress oracle which wants the matched wallet address
	/// @return address of the linked wallet
	function getLinkedWallet(address _oracleAddress) view public returns (address){
		return oracles[_oracleAddress].wallet;
	}
    
    
    
}