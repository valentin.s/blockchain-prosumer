#!/bin/bash
echo "script called"
####
#
# Uncomment the startup script according to what your raspberry pi is.
# This will unlock the matching wallet and oracle addresses to avoid performance issues.
#
####

#Producer
#/usr/local/bin/geth --datadir="/home/pi/blockchain-prosumer/blockchain/ethdata" --networkid 1337 --rpc --rpcapi="db,eth,net,web3,personal" --rpccorsdomain "*" --bootnodes="enode://dc23cebee14dce92208f12d09230c2dc31fccc5b639529318fcda0c4df89a3850aa42e2381e4122e37ed3577bd102490800e60bc1efdbcdc5b0b6130618ba3d7@192.168.1.1:30301" --unlock="0xfe3836212edf6cc87427c8ca685a9d0b28394720,0x7bc25df2333bca50a6084903f276ff9ad6373037" --password=/home/pi/blockchain-prosumer/blockchain/passwordfile --fast --cache=16 &

#Consumer1
#/usr/local/bin/geth --datadir="/home/pi/blockchain-prosumer/blockchain/ethdata" --networkid 1337 --rpc --rpcapi="db,eth,net,web3,personal" --rpccorsdomain "*" --bootnodes="enode://dc23cebee14dce92208f12d09230c2dc31fccc5b639529318fcda0c4df89a3850aa42e2381e4122e37ed3577bd102490800e60bc1efdbcdc5b0b6130618ba3d7@192.168.1.1:30301" --unlock="0x88ccc75109bab42062e40d11f4af9ef0e488679e,0x2773f066d34ce08cd737bbf6f7dfda4cbfa68414" --password=/home/pi/blockchain-prosumer/blockchain/passwordfile --fast --cache=16 &

#Consumer2
#/usr/local/bin/geth --datadir="/home/pi/blockchain-prosumer/blockchain/ethdata" --networkid 1337 --rpc --rpcapi="db,eth,net,web3,personal" --rpccorsdomain "*" --bootnodes="enode://dc23cebee14dce92208f12d09230c2dc31fccc5b639529318fcda0c4df89a3850aa42e2381e4122e37ed3577bd102490800e60bc1efdbcdc5b0b6130618ba3d7@192.168.1.1:30301" --unlock="0x3ee324b58d51d7be93191f2d332f14aa8bfed626,0x78f2089720cdb4764a30e799b98fda0a9f59b2db" --password=/home/pi/blockchain-prosumer/blockchain/passwordfile --fast --cache=16 &

#Prosumer1
#/usr/local/bin/geth --datadir="/home/pi/blockchain-prosumer/blockchain/ethdata" --networkid 1337 --rpc --rpcapi="db,eth,net,web3,personal" --rpccorsdomain "*" --bootnodes="enode://dc23cebee14dce92208f12d09230c2dc31fccc5b639529318fcda0c4df89a3850aa42e2381e4122e37ed3577bd102490800e60bc1efdbcdc5b0b6130618ba3d7@192.168.1.1:30301" --unlock="0x2ef995db69edf6791b1ab279083d90ef0a063194,0x2ce42acb894725fc7f18c7988a6fcc9794d9dac8" --password=/home/pi/blockchain-prosumer/blockchain/passwordfile --fast --cache=16 &

#Prosumer2
#/usr/local/bin/geth --datadir="/home/pi/blockchain-prosumer/blockchain/ethdata" --networkid 1337 --rpc --rpcapi="db,eth,net,web3,personal" --rpccorsdomain "*" --bootnodes="enode://dc23cebee14dce92208f12d09230c2dc31fccc5b639529318fcda0c4df89a3850aa42e2381e4122e37ed3577bd102490800e60bc1efdbcdc5b0b6130618ba3d7@192.168.1.1:30301" --unlock="0xb53a3276a6410dd3090ba1ada04453d25b03fa7d,0x37f4600a992c263192e37cf27dec4c7aff2ab074" --password=/home/pi/blockchain-prosumer/blockchain/passwordfile --fast --cache=16 &

#Auctionhouse
#/usr/local/bin/geth --datadir="/home/pi/blockchain-prosumer/blockchain/ethdata" --networkid 1337 --rpc --rpcapi="db,eth,net,web3,personal" --rpccorsdomain "*" --bootnodes="enode://dc23cebee14dce92208f12d09230c2dc31fccc5b639529318fcda0c4df89a3850aa42e2381e4122e37ed3577bd102490800e60bc1efdbcdc5b0b6130618ba3d7@192.168.1.1:30301" --fast --cache=16 &
read
